<?php

namespace CodeFlask\LaravelSavableConfig\Tests;

use CodeFlask\LaravelSavableConfig\Events\SettingDeleted;
use CodeFlask\LaravelSavableConfig\Events\SettingSaved;
use CodeFlask\LaravelSavableConfig\Models\Setting;
use CodeFlask\LaravelSavableConfig\Facades\Configuration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;

/**
 * Class LaravelSavableConfigTest
 *
 * @package CodeFlask\LaravelSavableConfig\Tests
 */
class LaravelSavableConfigTest extends BaseTestCase
{
    public function setUp()
    {
        parent::setUp();
        $this->loadPackageMigrations();
    }

    /**
     * 測試 Configuration::create 覆寫原生設定
     */
    public function testOverwriteRawConfigWhenConfigurationCreated()
    {
        // === ARRANGE ===
        $this->assertEquals('en', config('app.locale'));

        // === ACTION ===
        Event::fake();
        Setting::create([
            'key' => "app.locale",
            'value' => 'zh-Hant',
        ]);

        // === ASSERT ===
        Event::assertDispatched(SettingSaved::class);
        $this->assertEquals('zh-Hant', config('app.locale'));
    }

    /**
     * 測試 Configuration::updateOrCreate 覆寫原生設定
     */
    public function testOverwriteRawConfigWhenConfigurationUpdateOrCreate()
    {
        // === ARRANGE ===
        $this->assertEquals('en', config('app.locale'));

        // === ACTION ===
        Setting::updateOrCreate([
            'key' => "app.locale",
        ], [
            'value' => 'zh-Hant',
        ]);

        // === ASSERT ===
        $this->assertEquals('zh-Hant', config('app.locale'));
    }

    /**
     * 測試 Configuration 刪除後，回復原生設定
     */
    public function testRestoreRawConfigWhenConfigurationDeleted()
    {
        // === ARRANGE ===
        $this->assertEquals('en', config('app.locale'));
        $config = Setting::create([
            'key' => "app.locale",
            'value' => 'zh-Hant',
        ]);
        $this->assertEquals('zh-Hant', config('app.locale'));

        // === ACTION ===
        $config->delete();

        // === ASSERT ===
        $this->assertEquals('en', config('app.locale'));
    }

    /**
     * 測試套用來至資料庫的覆寫原生設定
     */
    public function testConfigurationApply()
    {
        // === ARRANGE ===
        $customConfigs = [
            // 原生 File Config 是 string
            'session.driver' => 'cookie',
            // 原生 File Config 是 numeric
            'session.lifetime' => 60,
            // 原生 File Config 是 boolean
            'session.encrypt' => true,
            // 原生 File Config 是 array
            'session.lottery' => [3, 50],
            // 原生 File Config 是 NULL
            'session.connection' => 'my-test',
            // 原生 File Config 是 Class
            'auth.providers.users.model' => Configuration::class,
            // 不存在於原生 File Config
            'session.nonexistent' => 'what',
        ];

        foreach ($customConfigs as $key => $value) {
            DB::table('settings')->insert([
                'key' => $key,
                'value' => json_encode($value),
                'updated_at' => date('c'),
                'created_at' => date('c')
            ]);
        }

        // === ACTION ===
        Configuration::apply();

        // === ASSERT ===
        $this->assertEquals('cookie', config('session.driver'));
        $this->assertEquals(60, config('session.lifetime'));
        $this->assertEquals(true, config('session.encrypt'));
        $this->assertArraySubset([3, 50], config('session.lottery'));
        $this->assertEquals('my-test', config('session.connection'));
        $this->assertEquals('what', config('session.nonexistent'));
        $this->assertEquals(Configuration::class, config('auth.providers.users.model'));
    }

    /**
     * 測試使用  Configuration::set() 來設定
     */
    public function testConfigurationSet()
    {
        // === ARRANGE ===
        $customConfigs = [
            // 原生 File Config 是 string
            'session.driver' => 'cookie',
            // 原生 File Config 是 numeric
            'session.lifetime' => 60,
            // 原生 File Config 是 boolean
            'session.encrypt' => true,
            // 原生 File Config 是 array
            'session.lottery' => [3, 50],
            // 原生 File Config 是 NULL
            'session.connection' => 'my-connection',
            // 不存在於原生 File Config
            'session.nonexistent' => 'what',
            // 原生 File Config 是 Class
            'auth.providers.users.model' => Configuration::class,
        ];

        foreach ($customConfigs as $key => $value) {
            Configuration::set($key, $value);
        }

        // === ACTION ===
        Configuration::apply();

        // === ASSERT ===
        $this->assertEquals('cookie', config('session.driver'));
        $this->assertEquals(60, config('session.lifetime'));
        $this->assertEquals(true, config('session.encrypt'));
        $this->assertArraySubset([3, 50], config('session.lottery'));
        $this->assertEquals('my-connection', config('session.connection'));
        $this->assertEquals('what', config('session.nonexistent'));
        $this->assertEquals(Configuration::class, config('auth.providers.users.model'));
    }
}
