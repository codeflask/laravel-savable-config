<?php

namespace CodeFlask\LaravelSavableConfig;

use Illuminate\Support\ServiceProvider;

class LaravelSavableConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes(
            [
                __DIR__ . '/../database/migrations' => database_path('migrations'),
            ],
            'laravel-savable-config'
        );

        if ($this->app->runningInConsole()) {
            // 讀取套件 migrations
            $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('configuration', Configuration::class);
    }
}
