<?php

namespace CodeFlask\LaravelSavableConfig\Models;

use CodeFlask\LaravelSavableConfig\Events\SettingDeleted;
use CodeFlask\LaravelSavableConfig\Events\SettingSaved;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var string
     */
    protected $primaryKey = 'key';

    /**
     * @var array
     */
    protected $fillable = [
        'key',
        'value'
    ];

    /**
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => SettingSaved::class,
        'deleted' => SettingDeleted::class,
    ];

    /**
     * @param  $value
     * @return mixed
     */
    public function getValueAttribute($value)
    {
        return json_decode($value, true);
    }

    /**
     * @param $value
     */
    public function setValueAttribute($value)
    {
        $this->attributes['value'] = json_encode($value);
    }

}
