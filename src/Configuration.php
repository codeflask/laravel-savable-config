<?php

namespace CodeFlask\LaravelSavableConfig;

use CodeFlask\LaravelSavableConfig\Models\Setting;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Schema;

class Configuration
{
    /**
     * 套用 DB Setting
     */
    public function apply()
    {
        // 以 DB Setting 來覆寫 config 設定
        if (Schema::hasTable('settings')) {
            foreach (Setting::all() as $setting) {
                $configKey = $setting->getAttribute('key');
                $rawValue = config($configKey);
                $newValue = $setting->getAttribute('value');

                if (is_array($newValue) && is_array($rawValue)) {
                    // 當 DB Setting 且 File Config 都是陣列，就以遞迴方式將 DB Setting 寫入到 File Config
                    // 這樣可以確保當 File Config 同層的 key 有新增設定值，才不會遺失新值
                    Config::set($configKey, array_replace_recursive($rawValue, $newValue));
                } elseif (gettype($rawValue) !== 'NULL') {
                    // 當 File Config 資料類型不是 null 時，DB Setting 要配合轉成對應的資料類型
                    settype($newValue, gettype($rawValue));
                    Config::set($configKey, $newValue);
                } else {
                    Config::set($configKey, $newValue);
                }
            }
        }
    }

    /***
     * 設定 DB Setting
     *
     * @param string $key 索引
     * @param mixed $value 設定值
     * @return mixed
     */
    public function set($key, $value)
    {
        return Setting::updateOrCreate(
            [
                'key' => $key,
            ],
            [
                'value' => $value,
            ]
        );
    }
}
