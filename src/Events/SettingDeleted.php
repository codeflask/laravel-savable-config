<?php

namespace CodeFlask\LaravelSavableConfig\Events;

use CodeFlask\LaravelSavableConfig\Models\Setting;
use Illuminate\Support\Facades\Config;

class SettingDeleted
{
    /**
     * 當「DB Setting」被刪除後，回填原生的「Config」
     *
     * SettingDeleted constructor.
     *
     * @param Setting $setting
     */
    public function __construct(Setting $setting)
    {
        $configKey = $setting->getAttribute('key');

        // 取得原生 File Config
        $configFileName = substr($configKey, 0, stripos($configKey, '.'));
        $configFilePath = realpath(config_path("{$configFileName}.php"));
        $configFileData = include $configFilePath;
        $rawValue = array_get($configFileData, ltrim($configKey, $configFileName . '.'));

        Config::set($configKey, $rawValue);
    }
}
