<?php

namespace CodeFlask\LaravelSavableConfig\Events;

use CodeFlask\LaravelSavableConfig\Models\Setting;
use Illuminate\Support\Facades\Config;

class SettingSaved
{
    /**
     * 當「DB Setting」儲存後，同步設定至 Config
     *
     * SettingSaved constructor.
     *
     * @param  Setting $setting
     * @throws \Exception
     */
    public function __construct(Setting $setting)
    {
        // 取得新生 DB Setting
        $configKey = $setting->getAttribute('key');
        $newValue = $setting->getAttribute('value');

        // 取得原生 File Config
        $configFileName = substr($configKey, 0, stripos($configKey, '.'));
        $configFilePath = realpath(config_path("{$configFileName}.php"));
        $configFileData = include $configFilePath;
        $rawValue = array_get($configFileData, ltrim($configKey, $configFileName . '.'));

        if (is_array($newValue) && is_array($rawValue)) {
            // 當 DB Setting 與 File Config 都是陣列，就以遞迴方式將 DB Setting 寫入到 File Config
            // 這樣可以確保當 File Config 之後同層的 key 有新增設定值，才不會遺失新值
            Config::set($configKey, array_replace_recursive($rawValue, $newValue));
        } elseif (gettype($rawValue) !== 'NULL') {
            // 當 File Config 資料類型不是 null 時，DB Setting 要配合轉成對應的資料類型
            settype($newValue, gettype($rawValue));
            Config::set($configKey, $newValue);
        } else {
            Config::set($configKey, $newValue);
        }
    }
}
