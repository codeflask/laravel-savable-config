### Installation

    composer install estus-flask/laravel-savable-config
    
After installing you should run the migrate command:

    php artisan migrate
    
You should add provider if laravel version < 5.5

    // config/app.php
    'providers' => [
        ...
        CodeFlask\LaravelSavableConfig\LaravelSavableConfigServiceProvider::class,
    ]
    
    
   
### Usage

Add `Configuration::apply()` to function `boot()` in file `AppServiceProvider.php` for apply the database settings

    class AppServiceProvider extends ServiceProvider
    {
        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            Configuration::apply();
        }
    }

Use `Configuration::set()` to make database setting mapping to raw file config

    Configuration::set("app.locale", 'zh-Hant');
    
### LICENSE
`laravel-savable-config` is released under the [MIT License]()